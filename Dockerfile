FROM python:3.10

ARG CONFIG

ENV PYTHONPATH="/opt/service_status_page"

RUN mkdir -p /opt/service_status_page /var/log/service_status_page /var/run/service_status_page 
RUN apt update -y
WORKDIR /opt/service_status_page
COPY requirements.txt .

RUN pip install --upgrade pip
RUN pip install -r requirements.txt

# Installing App
COPY . .
WORKDIR /opt/service_status_page/common/config
RUN ln -s config_${CONFIG}.py config.py
RUN chown -R www-data:www-data /opt/service_status_page /var/log/service_status_page /var/run/service_status_page

WORKDIR /opt/service_status_page



