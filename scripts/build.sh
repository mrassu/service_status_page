#!/bin/bash
set -e 

cd /opt/service_status_page

echo "WARNING: All uncommitted changes are going to be stashed. You'll have to unstash them manually after image building."
git stash
git pull

GIT_COMMIT=`git rev-parse --short HEAD`
docker build --build-arg CONFIG=prod -t service_status_page:$GIT_COMMIT .
docker tag service_status_page:$GIT_COMMIT service_status_page:latest
docker tag service_status_page:latest eu.gcr.io/goodbarber-prod/service_status_page:$GIT_COMMIT
docker tag service_status_page:latest eu.gcr.io/goodbarber-prod/service_status_page:latest
docker push eu.gcr.io/goodbarber-prod/service_status_page:$GIT_COMMIT
docker push eu.gcr.io/goodbarber-prod/service_status_page:latest
