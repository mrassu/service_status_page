#!/bin/bash

if [ ! -f /usr/bin/gcloud ]; then

    # Create an environment variable for the correct distribution
    export CLOUD_SDK_REPO="cloud-sdk-$(lsb_release -c -s)" 

    # Add the Cloud SDK distribution URI as a package source
    echo "deb http://packages.cloud.google.com/apt $CLOUD_SDK_REPO main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list

    # Import the Google Cloud Platform public key
    curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -

    # Update the package list and install the Cloud SDK
    sudo apt-get update && sudo apt-get -y install google-cloud-sdk  || exit 1

else
    echo "Gcloud package already installed."
fi

if [ ! -f /usr/local/bin/docker-compose ]; then
    curl -L https://github.com/docker/compose/releases/download/1.17.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
    chmod o+x /usr/local/bin/docker-compose
else
    echo "docker-compose already installed"
fi

ACTIVE_PROJECT=`gcloud info|grep Project`

if [ "$ACTIVE_PROJECT" == "Project: [None]" ]; then
    gcloud init --console-only || exit 1
else
    echo "Gcloud project already configured."
fi

gcloud auth configure-docker

exit 0

