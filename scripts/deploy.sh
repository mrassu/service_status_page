#!/bin/bash
set -e 
cd /opt/service_status_page

git pull origin master && ./scripts/init.sh && docker pull eu.gcr.io/goodbarber-prod/service_status_page:latest && docker-compose up -d