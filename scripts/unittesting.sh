#!/bin/bash

cd /opt/service_status_page
docker exec -e PYTHONPATH=$PYTHONPATH `docker ps --filter "name=service_status_page-preprod-uwsgi" -q` python manage.py test --noinput
