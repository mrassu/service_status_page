import json
from django.urls import reverse

from unittest.mock import patch

from rest_framework.test import APISimpleTestCase
from service_status_page.data import PAYLOAD_FROM_OPSGENIE, PAYLOAD_FROM_OPSGENIE_WRONG

from service_status_page.mapper import PingdomTagsToStatusPageComponentsMapper

from rest_framework import status

from service_status_page.mapper import PingdomTagsToStatusPageComponentsMapper

class WebhookTestCase(APISimpleTestCase):
    

    def setUp(self):
        self.start_patchers()
        self.addCleanup(patch.stopall)
        return super().setUp()

    def _fake_post(self, url, payload, headers={}):
        return payload
    
    def _fake_get(self, url, headers={}):
        return []

    def _fake_patch(self, url, payload, headers={}):
        return payload

    def _fake_authenticate(self, fake):
        return (None, None)


    def start_patchers(self):
        print("Patching to avoid request to API.")
        self.fakepost = patch('service_status_page.lib.BaseAPICaller._post', side_effect=self._fake_post)
        self.fakepatch = patch('service_status_page.lib.BaseAPICaller._patch', side_effect=self._fake_patch)
        self.fakeget = patch('service_status_page.lib.BaseAPICaller._get', side_effect=self._fake_get)
        self.no_cred = patch('service_status_page.auth.SecretKeyAuthentification.authenticate', side_effect=self._fake_authenticate)
        self.fakepost.start()
        self.fakepatch.start()
        self.fakeget.start()
        self.no_cred.start()

    def test_legit_webhook_commerce_and_classic_components(self):
        url = reverse("webhook-view")
        payload = PAYLOAD_FROM_OPSGENIE
        resp = self.client.post(url, data=payload, format="json")
        json_resp = json.loads(resp._container[0])
        excepted = {
            "status": "OK",
            "components_id": [
                PingdomTagsToStatusPageComponentsMapper.COMPONENTS_API_COMMERCE_PRODUCT,
                PingdomTagsToStatusPageComponentsMapper.COMPONENTS_API_CLASSIC_PRODUCT
            ],
            "components_status" : "operational",
            "incident_closed" : True,
            "incident_created": False
        }
        self.assertDictEqual(json_resp, excepted)
        self.assertEquals(resp.status_code, status.HTTP_200_OK)

    def test_wrong_webhoob(self):
        url = reverse("webhook-view")
        payload = PAYLOAD_FROM_OPSGENIE_WRONG
        resp = self.client.post(url, data=payload, format="json")
        self.assertEquals(resp.status_code, status.HTTP_400_BAD_REQUEST)

class MapperTestCase(APISimpleTestCase):

    def test_pingdom_tags_commerce_to_components_id(self):
        tags = ["api", "commerce"]
        res = PingdomTagsToStatusPageComponentsMapper().get_components_id(tags)
        self.assertEquals(len(res),1)

    def test_pingdom_tags_multi_to_components_id(self):
        tags = ["api", "commerce", "classic"]
        res = PingdomTagsToStatusPageComponentsMapper().get_components_id(tags)
        self.assertEquals(len(res),2)

    def test_pingdom_tags_none_to_components_id(self):
        tags = ["api", "fefe", "clafeffefessic"]
        res = PingdomTagsToStatusPageComponentsMapper().get_components_id(tags)
        self.assertEquals(len(res),0)
    
    
        