from django.urls import path
from service_status_page import views

urlpatterns = [
    path("webhook/",
         views.OpsGenieWehbookHandler.as_view(), name="webhook-view"),
]
