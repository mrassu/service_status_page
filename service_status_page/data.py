PAYLOAD_FROM_OPSGENIE = {
            "action":   "Close",
            "alert":{
                "alertId":"19b8abec-41be-4640-b781-4b589348bcf3-1649256748703",
                "message":"** PROBLEM Service Alert: Gate/fake check_raid test is CRITICAL **",
                "tags":[
                    "api",
                    "classic",
                    "commerce"
                ],
                "tinyId":"24",
                "entity":"",
                "alias":"Gate_fake check_raid test",
                "createdAt":"1649256748703",
                "updatedAt":"1649256749472000000",
                "username":"Nagios",
                "description":"Notification Type: PROBLEM\nService: fake check_raid test\nHost: Gate\nAddress: 10.0.0.254\nState: CRITICAL\nAdditional Info: XXXX\nDate/Time: Wed Apr 6 16:52:28 CEST 2022",
                "team":"Backend",
                "responders":[
                    {
                        "id":"ff907811-e4c6-4235-ab51-a6c9268ba2c9",
                        "type":"team",
                        "name":"Backend"
                    }
                ],
                "teams":[
                    "ff907811-e4c6-4235-ab51-a6c9268ba2c9"
                ],
                "actions":[
                    
                ],
                "details":{
                    "host_address":"10.0.0.254",
                    "host_alias":"Gate",
                    "host_attempt":"1",
                    "host_duration":"1d 11h 52m 37s",
                    "host_group_name":"Linux-SNMP-GROUP",
                    "host_latency":"0.000",
                    "host_name":"Gate",
                    "host_output":"OK - 10.0.0.254 rta 36.949ms lost 0%",
                    "host_perf_data":"rta=36.949ms;3000.000;5000.000;0; pl=0%;80;100;0;100 rtmax=45.924ms;;;; rtmin=30.995ms;;;;",
                    "host_state":"UP",
                    "host_state_type":"HARD",
                    "last_host_check":"1649256486",
                    "last_host_state_change":"1649127591",
                    "last_service_check":"1649256748",
                    "last_service_state_change":"1649256748",
                    "max_host_attempts":"5",
                    "max_service_attempts":"HARD",
                    "nagios_server":"default",
                    "service_attempt":"1",
                    "service_desc":"fake check_raid test",
                    "service_duration":"0d 0h 0m 0s",
                    "service_group_name":"$",
                    "service_latency":"0.348",
                    "service_output":"XXXX",
                    "service_state":"CRITICAL",
                    "service_state_type":"HARD"
                },
                "priority":"P3",
                "source":"NagiosXIV2"
            },
            "source":{
                "name":"",
                "type":"NagiosXIV2"
            },
            "integrationName":"Webhook",
            "integrationId":"cf050e49-7e3e-4ae3-99f8-922024d3989c",
            "integrationType":"Webhook"
}

PAYLOAD_FROM_OPSGENIE_WRONG = {
            "action":   "Create",
            "alert":{
                "alertId":"19b8abec-41be-4640-b781-4b589348bcf3-1649256748703",
                "message":"** PROBLEM Service Alert: Gate/fake check_raid test is CRITICAL **",
                "tags":[
                    "api",
                    "classic",
                    "commerce"
                ],
                "tinyId":"24",
                "entity":"",
                "alias":"Gate_fake check_raid test",
                "createdAt":"1649256748703",
                "updatedAt":"1649256749472000000",
                "username":"Nagios",
                "description":"Notification Type: PROBLEM\nService: fake check_raid test\nHost: Gate\nAddress: 10.0.0.254\nState: CRITICAL\nAdditional Info: XXXX\nDate/Time: Wed Apr 6 16:52:28 CEST 2022",
                "team":"Backend",
                "responders":[
                    {
                        "id":"ff907811-e4c6-4235-ab51-a6c9268ba2c9",
                        "type":"team",
                        "name":"Backend"
                    }
                ],
                "teams":[
                    "ff907811-e4c6-4235-ab51-a6c9268ba2c9"
                ],
                "actions":[
                    
                ],
                "details":{
                    "host_address":"10.0.0.254",
                    "host_alias":"Gate",
                    "host_attempt":"1",
                    "host_duration":"1d 11h 52m 37s",
                    "host_group_name":"Linux-SNMP-GROUP",
                    "host_latency":"0.000",
                    "host_name":"Gate",
                    "host_output":"OK - 10.0.0.254 rta 36.949ms lost 0%",
                    "host_perf_data":"rta=36.949ms;3000.000;5000.000;0; pl=0%;80;100;0;100 rtmax=45.924ms;;;; rtmin=30.995ms;;;;",
                    "host_state":"UP",
                    "host_state_type":"HARD",
                    "last_host_check":"1649256486",
                    "last_host_state_change":"1649127591",
                    "last_service_check":"1649256748",
                    "last_service_state_change":"1649256748",
                    "max_host_attempts":"5",
                    "max_service_attempts":"HARD",
                    "nagios_server":"default",
                    "service_attempt":"1",
                    "service_desc":"fake check_raid test",
                    "service_duration":"0d 0h 0m 0s",
                    "service_group_name":"$",
                    "service_latency":"0.348",
                    "service_output":"XXXX",
                    "service_state":"CRITICAL",
                    "service_state_type":"HARD"
                },
                "priority":"P3",
                "source":"NagiosXIV2"
            },
            "source":{
                "name":"",
                "type":"NagiosXIV2"
            },
            "integrationName":"Webhook",
            "integrationId":"cf050e49-7e3e-4ae3-99f8-922024d3989c",
            "integrationType":"Webhook"
}
