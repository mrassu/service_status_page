import json

from common import settings

from django.http import JsonResponse

from rest_framework.views import APIView
from rest_framework import status

from service_status_page.auth import SecretKeyAuthentification
from service_status_page.lib import StatusPageAPICaller

from service_status_page.mapper import PingdomTagsToStatusPageComponentsMapper


class OpsGenieWehbookHandler(APIView):

    authentication_classes = [SecretKeyAuthentification,]

    ACTION_ACKNOWLEDGE = "Acknowledge"
    ACTION_CLOSE = "Close"
    

    OPS_GENIE_ACTION_TO_STATUS_PAGE_STATUS = {
        ACTION_ACKNOWLEDGE : "partial_outage",
        ACTION_CLOSE: "operational",
    }

    INCIDENT_TITLE = "Partial outage."
    INCIDENT_BODY = (
                        "Our monitoring systems have detected a partial outage "
                        "on one or more of our services. Our engineers are "
                        "currently investigating the issue. More information "
                        "will be given soon."
                    )


    def need_to_create_incidents(self, action):
        if action == self.ACTION_ACKNOWLEDGE:
            if not self.get_unresolved_incidents_on_status_page():
                return True
        return False
    
    def need_to_close_incidents(self, action):
        # Return True if all components are operational
        if action == self.ACTION_CLOSE:
            components = self.get_components_list()
            for component in components:
                status = component.get('status')
                if status != "operational":
                    name = component.get('name')
                    return False
        else:
            return False
        return True

    def get_components_list(self):
        return StatusPageAPICaller(page_id=settings.PAGE_ID).get_components_list()


    def get_unresolved_incidents_on_status_page(self):
        return StatusPageAPICaller(page_id=settings.PAGE_ID).get_unresolved_incidents()
        

    def create_incident_on_status_page(self):
        return StatusPageAPICaller(page_id=settings.PAGE_ID).create_incident(self.INCIDENT_TITLE, self.INCIDENT_BODY)


    def close_all_incidents_on_status_page(self):
        unresolved_incidents = self.get_unresolved_incidents_on_status_page()
        for incident in unresolved_incidents:
            self.close_incidents_on_status_page(incident_id=incident.get('id'))


    def close_incidents_on_status_page(self, incident_id):
        return StatusPageAPICaller(page_id=settings.PAGE_ID).close_incident(incident_id=incident_id)


    def update_component_status(self, component_id, status):
        print(f"Will change component id {component_id} to status:{status}")
        return StatusPageAPICaller(page_id=settings.PAGE_ID).change_component_status(component_id=component_id, status=status)
        

    def post(self, request, *args, **kwargs):
        try:
            data = json.loads(request.body)
            alert = data['alert']
            tags = alert['tags']
            action = data['action']
            component_status = self.OPS_GENIE_ACTION_TO_STATUS_PAGE_STATUS[action]
        except Exception as e:
            print(f"Error, body received : {request.body}")
            return JsonResponse(
                data={
                    "status": "KO",
                    "reason": "Error on key-value "+str(e)
                    },
                    status=status.HTTP_400_BAD_REQUEST
                )
        incident_created = False
        incident_closed = False
        components_id = PingdomTagsToStatusPageComponentsMapper().get_components_id(tags=tags)
        for component_id in components_id:
            self.update_component_status(component_id=component_id, status=component_status)
        
        if self.need_to_create_incidents(action=action):
            incident_created = True
            self.create_incident_on_status_page()
        
        if self.need_to_close_incidents(action=action):
            incident_closed = True
            self.close_all_incidents_on_status_page()

        return JsonResponse(
            data={
                "status": "OK",
                "components_id":components_id,
                "components_status": component_status,
                "incident_created": incident_created,
                "incident_closed": incident_closed
                },
                status=status.HTTP_200_OK
            )
          



