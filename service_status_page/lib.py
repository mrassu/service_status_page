from abc import ABC
from django.conf import settings
import requests

from common.settings import STATUS_PAGE_API_KEY

class BaseAPICaller(ABC):

    TS_KEY = "__TIME__"
    DATA_KEY = "__DATA__"

    def __init__(self, page_id):
        self.page_id = page_id


    def _get(self, endpoint, headers={}, params={}, retries=3, timeout=10, cache_ttl=60):
        request_url = f"{self.BASE_API_URL}{endpoint}"
        while retries > 0:
            try:
                response = requests.get(request_url, headers=headers,
                                        params=params, timeout=timeout)
                if response.status_code == 200:
                    return response.json()
            except requests.exceptions.Timeout:
                pass
            retries -= 1
        return None

    def _patch(self, endpoint, payload, headers={}, params={}, retries=3, timeout=10, cache_ttl=60):
        request_url = f"{self.BASE_API_URL}{endpoint}"
        while retries > 0:
            try:
                response = requests.patch(request_url, headers=headers,
                                          json=payload, params=params, timeout=timeout)
                if response.status_code == 200:
                    return response.json()
            except requests.exceptions.Timeout:
                pass
            retries -= 1
        return None

    def _post(self, endpoint, payload, headers={}, params={}, retries=3, timeout=10, cache_ttl=60):
        request_url = f"{self.BASE_API_URL}{endpoint}"
        while retries > 0:
            try:
                response = requests.post(request_url, json=payload,
                                         headers=headers, params=params, timeout=timeout)
                if response.status_code == 201:
                    return response.json()
            except requests.exceptions.Timeout:
                pass
            retries -= 1
        return None


class StatusPageAPICaller(BaseAPICaller):

    BASE_API_URL = settings.STATUS_PAGE_API_URL_V1
    PAGE_ID = settings.PAGE_ID
    STATUS_PAGE_API_KEY = settings.STATUS_PAGE_API_KEY


    def get_headers(self):
        return {
            "Authorization": "OAuth "+STATUS_PAGE_API_KEY
        }


    def get_unresolved_incidents(self):
        url = f"/pages/{self.PAGE_ID}/incidents/unresolved"
        return self._get(url, headers=self.get_headers())


    def get_components_list(self):
        url = f"/pages/{self.PAGE_ID}/components/"
        return self._get(url, headers=self.get_headers())

    
    def change_component_status(self, component_id, status):
        url = f"/pages/{self.PAGE_ID}/components/{component_id}"
        payload = {
                "component": {
                "status": status
                }
        }
        return self._patch(url, payload, headers=self.get_headers())

    
    def create_incident(self, incident_name, incident_message):
        url = f"/pages/{self.PAGE_ID}/incidents"
        payload = {
            "incident": {
                "name": incident_name,
                "status": "investigating",
                "body": incident_message,
            }
        }
        return self._post(url, payload, headers=self.get_headers())

    
    def close_incident(self, incident_id):
        url = f"/pages/{self.PAGE_ID}/incidents/{incident_id}"
        payload = {
            "incident": {
                "status": "resolved", 
            }
        }
        return self._patch(url, payload, headers=self.get_headers())