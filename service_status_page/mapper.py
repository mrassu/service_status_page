from common import settings

class PingdomTagsToStatusPageComponentsMapper():

    #PINGDOM TAGS NAMES
    API = "api"
    COMMERCE = "commerce"
    CLASSIC = "classic"
    BACKENDS = "backends"
    PWA = "pwa"
    BACKOFFICES = "backoffices"
    WEBSITES = "websites"

    #TEST ACCOUNT 
    #COMPONENTS_API_COMMERCE_PRODUCT = "cr5ky09wx09c"
    #COMPONENTS_API_CLASSIC_PRODUCT = "rgg8jwg06rr5"
    #COMPONENTS_BACKENDS = "74mqhbz7vlpq"
    #COMPONENTS_PWA = "kkbz1k654cf4"
    #COMPONENTS_BACKOFFICES = "t79tgpk74tqd"
    #COMPONENTS_WEBSITES = "4tvlm58y0fhg"

    #PROD
    COMPONENTS_API_COMMERCE_PRODUCT = "k7wzqwpzrcm9"
    COMPONENTS_API_CLASSIC_PRODUCT = "3csqlsqplrxl"
    COMPONENTS_BACKENDS = "2n572x9syq94"
    COMPONENTS_PWA = "9zqqs9g87r8y"
    COMPONENTS_BACKOFFICES = "5xnppjwsn0k7"
    COMPONENTS_WEBSITES = "4g5vwkbl1m5q"

    rules = [
        ({API, COMMERCE}, COMPONENTS_API_COMMERCE_PRODUCT),
        ({API, CLASSIC}, COMPONENTS_API_CLASSIC_PRODUCT),
        ({BACKENDS}, COMPONENTS_BACKENDS),
        ({PWA}, COMPONENTS_PWA),
        ({BACKOFFICES}, COMPONENTS_BACKOFFICES),
        ({WEBSITES}, COMPONENTS_WEBSITES),

    ]

    def get_components_id(self, tags):
        components = []
        tags = [tag.lower() for tag in tags]
        for rule, component in self.rules:
            if not rule - set(tags):
                components.append(component)
        return components
