from rest_framework import authentication
from rest_framework.exceptions import AuthenticationFailed
from common import settings

class SecretKeyAuthentification(authentication.BaseAuthentication):

    def authenticate(self, request):
        if request.META.get('HTTP_SECRET_KEY') == settings.SECRET_KEY:
            return (None, None)
        else:
            raise AuthenticationFailed()
