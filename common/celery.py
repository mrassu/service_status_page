from celery import Celery
from django.conf import settings
# from celery.schedules import crontab
import os

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'common.settings')


app = Celery(
    "service_status_page", broker=settings.CELERY_BROKER_URL,
    backend=settings.CELERY_RESULT_BACKEND
)

app.config_from_object("django.conf:settings")
app.autodiscover_tasks()


app.conf.beat_schedule = dict()
