from django.urls import path
from common.views import test_view
from django.urls import include


urlpatterns = [
    path("test/", test_view),
    path("statuspage/v1/", include("service_status_page.urls")),
]
