
class ServiceStatuspageConfig():

    DEBUG = True

    TEST_ENVIRONMENT = True
    LOG_LEVEL = "INFO"
    LOG_FILE = '/var/log/service_status_page/service_status_page.log'

    SECRET_KEY = "f03h26jb*e5__i#)8ar@9q(savx&p2ju9p4@j7!1)+)5r!165)"

    PAGE_ID = "r7shykxgydb1"
    STATUS_PAGE_API_KEY = "db7a0d2a-be49-4a76-a046-8bd1d30ba70f"
    STATUS_PAGE_API_URL_V1 = "https://api.statuspage.io/v1/"
